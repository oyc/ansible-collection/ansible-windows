"""
Invoke tasks file to manage ansible playbooks

This file can be seen as a Python equivalent of a Makefile.
"""

from invoke import task, run

@task
def play(context, inventory="windows_hosts.yml", playbook="test-playbook.yml"):
    """
    Run given playbook with any given inventory.
    - Playbook must exist in "./playbooks/" directory
    - Inventory must exist in "./inventories/" directory

    Default uses windows_hosts.yml as inventory and test-playbook.yml as playbook.
    """
    run("ansible-playbook -i inventories/{0} playbooks/{1} --vault-id dev@prompt".format(
        inventory,
        playbook)
    )
